# Ansible Role: Terraformer

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-terraformer/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-terraformer/-/commits/main) 

This role installs [Terraformer](https://github.com/GoogleCloudPlatform/terraformer/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terraformer_version: latest # tag 0.8.17 if yoy want a specific version
    terraformer_provider: google # more providers on: https://github.com/GoogleCloudPlatform/terraformer/releases
    setup_dir: /tmp
    terraformer_bin_path: /usr/local/bin/terraformer
    terraformer_repo_path: https://github.com/GoogleCloudPlatform/terraformer/releases/download

This role can install the latest or a specific version. See [available Terraformer releases](https://github.com/GoogleCloudPlatform/terraformer/releases/) and change this variable accordingly.

This variable allows to choice a specific provider to build IaC code with existing insfrastructure. The value "all" will download a single binary with all the provider embedded.

    terraformer_provider: google

The path of the Terraformer repository.

    terraformer_repo_path: https://github.com/GoogleCloudPlatform/terraformer/releases/download

The location where the terraformer binary will be installed.

    terraformer_bin_path: /usr/local/bin/terraformer

Terraformer needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Terraformer. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: terraformer

## License

MIT / BSD
